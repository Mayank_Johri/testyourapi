// // Copyright (c) 2015-2017 Jeevanandam M. (jeeva@myjeeva.com), All rights reserved.
// // resty source code and usage is governed by a MIT style
// // license that can be found in the LICENSE file.
//
// package main
//
// import (
// 	"github.com/go-resty/resty"
// )
//
//
// func Main() {
//   Resty r = new Resty();
//   Object name = r.json("http://ws.geonames.org/postalCodeLookupJSON?postalcode=66780&country=DE").get("postalcodes[0].placeName");
// }

import "github.com/dghubble/sling"

/* code snippets needed
1. read/write following format
  a. sqlite
  b. YAML
  c. json
  d. text
  e. csv
2. REST client code for
  a. get
  b. post
  c. del
  d. put
3.
*/
