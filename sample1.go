まずは GET リクエストをやってみよう。

package main

import (
    "net/http"
    "io/ioutil"
)

func main() {

    response, _ := http.Get("http://httpbin.org/get")
    body, _ := ioutil.ReadAll(response.Body)
    defer response.Body.Close()

    println(string(body))

}
Cookie やヘッダーを指定したいのであれば、http.NewRequest を使う。Basic 認証のユーザーとパスワードは SetBasicAuth を使って指定できる。

package main

import (
    "net/http"
    "net/http/cookiejar"
    "io/ioutil"
)

func main() {

    cookieJar, _ := cookiejar.New(nil)

    client := &http.Client {
        Jar: cookieJar,
    }

    req, _:= http.NewRequest("GET", "http://httpbin.org/basic-auth/myuser/mypasswd", nil)
    req.Header.Set("User-Agent", "My User Agent")
    req.SetBasicAuth("myuser", "mypasswd")
    res, _ := client.Do(req)
    body, _ := ioutil.ReadAll(res.Body)
    defer res.Body.Close()

    println(string(body))
}
次は POST リクエストをやってみよう。

package main

import (
    "net/http"
    "net/url"
    "io/ioutil"
)

func main() {

    resp, _ := http.PostForm(
        "http://httpbin.org/post",
        url.Values{"foo": {"bar"}, },
    )

    body, _ := ioutil.ReadAll(resp.Body)
    defer resp.Body.Close()

    println(string(body))
}
Client.Post で投稿してみよう。

package main

import (
    "net/http"
    "net/url"
    "strings"
    "io/ioutil"
)

func main() {

    client := &http.Client{}
    data := url.Values{"foo": {"bar"}}

    resp, _ := client.Post(
        "http://httpbin.org/post",
        "application/x-www-form-urlencoded",
        strings.NewReader(data.Encode()),
    )
    body, _ := ioutil.ReadAll(resp.Body)
    defer resp.Body.Close()

    println(string(body))
}
http.NewRequest を使う場合、次のように書ける。

package main

import (
    "net/http"
    "net/url"
    "strings"
    "io/ioutil"
)

func main() {

    client := &http.Client{}
    data := url.Values{"foo": {"bar"}}

    req, _ := http.NewRequest(
        "POST",
        "http://httpbin.org/post",
        strings.NewReader(data.Encode()),
     )
    req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

    resp, _ := client.Do(req)
    body, _ := ioutil.ReadAll(resp.Body)
    defer resp.Body.Close()

    println(string(body))
}
今度は Twitter のアプリケーション限定の認証(Application-only authentication Overview)で使う Bearer トークンを申請してみよう。

package main

import (
  "net/http"
  "net/url"
  "encoding/base64"
  "strings"
  "io/ioutil"
  "encoding/json"
)

func main() {
  key := "xxxxxxx"
  secret := "xxxxxxx"
  token := base64.StdEncoding.EncodeToString([]byte(url.QueryEscape(key) + ":" + url.QueryEscape(secret)))

  client := &http.Client{}
  data := url.Values{"grant_type": {"client_credentials"}}

  req, _ := http.NewRequest(
    "POST",
    "https://api.twitter.com/oauth2/token",
    strings.NewReader(data.Encode()),
  )
  req.Header.Set("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
  req.Header.Set("Authorization", "Basic " + token)

  resp, _ := client.Do(req)
  body, _ := ioutil.ReadAll(resp.Body)
  defer resp.Body.Close()j

  var f interface{}
  json.Unmarshal(body, &f)
  m := f.(map[string]interface{})

  println(m["access_token"].(string))
}
